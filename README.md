# LSTM Neural Network for Time Series Prediction

使用 LSTM 来预测股票收盘价。

## Requirements

安装 requirements.txt 文件.

* Python 3.5.x
* TensorFlow 1.10.0
* Numpy 1.15.0
* Keras 2.2.2
* Matplotlib 2.2.2

## 参考
https://github.com/jaungiers/LSTM-Neural-Network-for-Time-Series-Prediction

